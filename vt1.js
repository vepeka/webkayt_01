/* jshint esversion: 10 */
"use strict";

// Joukkueen sarja on viite data.sarjat-taulukossa lueteltuihin sarjoihin
// Joukkueen rastileimausten rastit ovat viitteitä data.rastit-Objektissa oleviin rasteihin

// Kirjoita tästä eteenpäin oma ohjelmakoodisi

/**
  * Taso 1
  * Järjestää leimaustavat aakkosjärjestykseen
  * isoilla ja pienillä kirjaimilla ei ole järjestämisessä merkitystä (case insensitive).
  * Leimaustavan nimen alussa tai lopussa olevalle whitespacella ei myöskääm ole merkitystä. "  foo " on siis sama kuin "foo"
  * Alkuperäistä rakennetta ei saa muuttaa tai korvata vaan järjestäminen tehdään alkup. taulukon kopiolle.
  * Järjestetty lista leimaustavoista näkyy sivulla olevalla lomakkeella
  * @param {Array} leimaustavat-taulukko, jonka kopio järjestetään
  * @return {Array} palauttaa järjestetyn _kopion_ leimaustavat-taulukosta
*/
function jarjestaLeimaustavat(leimaustavat) {
    //console.log("jarjestaLeimaustavat", leimaustavat);
    let sortedLeimaukset = Array.from(leimaustavat);
    sortedLeimaukset.sort(sortString);
    //console.log("leimaukset", sortedLeimaukset);
    return sortedLeimaukset; // tässä pitää palauttaa järjestetty kopio eikä alkuperäistä
}
/**
 * Järjestelee stringejä.
 * Muuttaa isoiksi kirjaimiksi, trimmaa tyhjät takaa ja edestä.
 * Katsoo, että stringit ovat kelpoja.
 * Ja lopuksi tekee vertailut.
 * @param {String} a
 * @param {String} b
 * @return {int} 
 */
function sortString(a, b) {
    let aa = a.trim().toLocaleUpperCase("fi"); // trimmataan päistä, isot kirjaimet ja vertaillaan vasta sitten.
    let bb = b.trim().toLocaleUpperCase("fi"); // ehkä siinä säästä jotain, jos sen tekee alussa kerran, eikä jokaisessa if?
    //console.log("testi trim locale", aa, bb);
    
    if (!aa) {
	// console.log("ei kelpaa?", a);
	return 1; // jos undefined, niin vikaksi?
    }

    if (aa.length === 0) {
	return 1; // jos pituus nolla, niin vikaksi?
    }

    if (aa < bb) {
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/toLocaleUpperCase
	// console.log("a pienempi", a, b);
	return -1; // a jos pienempi, niin eteen
    }

    if (aa  > bb) {
	// console.log("b pienempi", a, b);
	return 1; // a jos suurempi, niin perälle.
    }

    //console.log("samat?", a, b);
    return 0; // jos samat, niin samat.
}

/**
 * Taso 1
 * Järjestää sarjat aakkosjärjestykseen sarjan nimen perustella
 * isoilla ja pienillä kirjaimilla ei ole järjestämisessä merkitystä (case insensitive)
 * Alkuperäistä rakennetta ei saa muuttaa tai korvata vaan järjestäminen tehdään alkup. taulukon kopiolle.
 * Järjestetetyt sarjat näkyvät sivulla olevalla lomakkeella
 * @param {Array} taulukko, jonka kopio järjestetään
 * @return {Array} palauttaa järjestetyn _kopion_ sarjat-taulukosta
 */
function jarjestaSarjat(sarjat) {
    //console.log("jarjestaSarjat", sarjat);
    //console.log("sarjat, testi", sarjat[0].nimi.trim().toLocaleUpperCase());

    let sortedSarjat = Array.from(sarjat);
    sortedSarjat.sort(sortSarjatNimet);
    //console.log("sortedSarjat", sortedSarjat);
    return sortedSarjat;  // tässä pitää palauttaa järjestetty kopio eikä alkuperäistä
}

/**
 *
 *järjestää sarjojen nimet aakkojärjestykseen
 * @param {String} a
 * @param {String} b
 * @return {int} 
   */
function sortSarjatNimet(a, b) {
    let aa = a.nimi.trim();
    aa = aa.toLocaleUpperCase("fi");
    let bb = b.nimi.trim();
    bb = bb.toLocaleUpperCase("fi");

    if (!aa) {
	// console.log("ei kelpaa?", a.nimi);
	return 1; // jos undefined nimi, niin vikaksi?
    }

    if (aa.length === 0) {
	return 1; // jos pituus nolla, niin vikaksi?
    }

    if (aa < bb) {
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/toLocaleUpperCase
	// console.log("a pienempi", a.nimi, b.nimi);
	return -1; // a jos pienempi, niin eteen
    }

    if (aa > bb) {
	// console.log("b pienempi", a.nimi, b.nimi);
	return 1; // a jos suurempi, niin perälle.
    }

    // console.log("samat?", a.nimi, b.nimi);
    return 0; // jos samat, niin samat.
}

/**
 * Taso 1
 * Lisää uuden sarjan ja palauttaa tiedon onnistuiko lisääminen vai ei
 * Sarja lisätään vain jos kaikki seuraavat ehdot täyttyvät:
 *  - Toista samannimistä sarjaa ei ole olemassa. Nimien vertailussa ei huomioida isoja ja pieniä kirjaimia tai nimen alussa ja lopussa välilyöntejä etc. (whitespace). Nimien vertailu on siis caseinsensitive.
 * - "  Foo " on siis sama kuin "foo"
 * - sarjan nimi ei voi olla pelkkää whitespacea.
 * - Sarjan keston täytyy olla kokonaisluku ja suurempi kuin 0
 * - Sarjan uniikki id on luotava seuraavalla tavalla: Käy läpi kaikki sarjat ja etsi suurin id, lisää tähän 1
 *  Uusi sarja tallennetaan sarjat-taulukkoon. Sarjan on oltava seuraavaa muotoa:
 *  {
 *     "id": {Number}, // Jokaisella sarjalle oleva uniikki kokonaislukutunniste, pakollinen tieto
 *     "nimi": {String}, // Sarjan uniikki nimi, pakollinen tieto
 *     "kesto": {Number}, // sarjan kesto tunteina, pakollinen tieto
 *     "alkuaika": {String}, // Sarjan alkuaika, oletuksena ""
 *     "loppuaika": {String}, // Sarjan loppuaika, oletuksena ""
 *     "joukkueet": {Array}, // Taulukko sarjaan kuuluvista joukkueista. Oletuksena tyhjä taulukko []
 *  }
 * Tätä funktiota voi kokeilla, kun lisää sivulla olevalla lomakkeella uuden sarjan
 * @param {Array} sarjat - taulukko johon sarja lisätään
 * @param {String} nimi - Lisättävän sarjan nimi
 * @param {String} kesto - Sarjan kesto merkkijonona
 * @param {String} alkuaika - Sarjan alkuaika, ei pakollinen
 * @param {String} loppuaika - Sarjan loppuaika, ei pakollinen
 * @return {Boolean} palauttaa true, jos sarja lisättiin tai false, jos lisäämistä ei tehty
 */
function lisaaSarja(sarjat, nimi, kesto, alkuaika, loppuaika) {
    //console.log("lisaaSarja", sarjat);
    //console.log("lisaaSarja NIMI", nimi);

    if (!tarkistaSarjaNimi(nimi, sarjat)) { // tarkistetaan sarjan nimi, jos ei kelpaa (false), niin palautetaan heti false.
	//console.log("nimi ei kelpaa");
	return false;
    }

    if (!tarkistaSarjaKesto(kesto)) { // tarkistetaan sarjan kesto, jos ei kelpaa (false), niin palautetaan heti false.	
	//console.log("kesto ei kelpaa");
	return false;
    }

    // en ole varma tarvitseeko näitä.
    let lisaaId = annaUusiId(sarjat); // tehdään id.
    //console.log("sarjaID?", lisaaId);
    let lisaaNimi = nimi; // hyväksytty nimi.
    let lisaaKesto = parseInt(kesto); // hyväksytty kesto inttinä.
    let lisaaAlkuaika = alkuaika; // tyhjä tai annettu? Miksei tarkasteta?
    let lisaaLoppuaika = loppuaika; // ??
    let lisaaJoukkueet = []; // tyhjä lista pyydetty.
    
    let uusiSarja = {
        nimi: lisaaNimi,
	id: lisaaId,
	kesto: lisaaKesto,
	alkuaika: lisaaAlkuaika,
	loppuaika: lisaaLoppuaika,
	joukkueet: lisaaJoukkueet
    }; // ohjeissa väärä järjestys kuin edelliset sarjat
    // edelliset sarjat on nimi, kesto, id ja loput..

    sarjat.push(uusiSarja);
    //console.log("lisaaSarja tehty?", sarjat);
    return true; 
}
/**
   * Tarkistaa, että nimi kelpaa uuden sarjan nimeksi.
   * @param {String} nimi
   * @param {Array} sarjat
   * @return {Boolean} 
   */
function tarkistaSarjaNimi(nimi, sarjat) {
    //let namn = nimi.trim().toLocaleUpperCase("fi");
    // huhhuh! mutta toimii.
    // ei muuten toiminut.
    let namn = nimi.trim();
    namn = namn.toLocaleUpperCase("fi");
    //console.log("nimikö?", namn);

    if (namn.length === 0) { //onko tyhjä?
	return false;
    }

    if (!namn) { //undefined?
	return false;
    }

    for (let sarja of sarjat) {
        let sarjaNimi = sarja.nimi.trim();
        sarjaNimi = sarjaNimi.toLocaleUpperCase("fi");
	if (sarjaNimi === namn) {
	    return false; // jos nimi jo löytyy, niin false
	}
    }

    return true; // jos ei tyhjä, eikä löydy jo, niin true
}

/**
   * Tarkistaa, että tarjotun sarjan kesto on sopiva.
   * @param {String} kesto
   * @return {Boolean}
   */
function tarkistaSarjaKesto(kesto) {
    if (Number.isSafeInteger(parseInt(kesto))) { // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isSafeInteger
	return true;
    }
    return false;
}

/**
 * palauttaa uuden uuden idn
 * käy läpi löytyvät idt ja palauttaa yhden suuremman, kuin suurin.
 * @param {Array} listaus objekteja, missä oltava id
 * @return {int} id
   */
function annaUusiId(listaus) {
    let id = 0;
    for (let objekti of listaus) {
        //console.log("objekti idtä varten", objekti, "ja", objekti.id);
	if (objekti.id >= id) {
            //console.log("isompi id löyty: ",  objekti.id);
            //console.log("WANHA id oli: ",  id);
            id = objekti.id + 1;
            //console.log("UUSI id on nyt: ",  id);
	}
    }
    return id;
}

/**
  * Taso 1
  * Poistaa joukkueen id:n perusteella data-rakenteesta ja palauttaa muuttuneen datan
  * @param {Object} joukkueet - taulukko josta joukkue poistetaan
  * @param {String} id - poistettavan joukkueen id
  * @return {Boolean} true, jos poisto onnistui tai false, jos poistettavaa joukkuetta ei löytynyt
  */
function poistaJoukkue(joukkueet, id) {
    //console.log("poistaJoukkue", joukkueet);
    if (!tarkistaId(id)) {
        return false; // tarkistaa onko id validi ja tarvittaessa katkoo heti.
    }
    let idx = parseInt(id);
    for (let i = 0; i < joukkueet.length; i++) {
        if (joukkueet[i].id === idx) {
            joukkueet.splice(i, 1);
            return true;
        }
    }
    return false;
}

/**
 * tarkistaa onko id validi, ihan oikea numero.
 * palauttaa true, jos löytyy ja false, jos ei löydy.
 * @param {String} id
 * @return {Boolean}
 */
function tarkistaId(id) {
    if (!/^[0-9]+$/.test(id)) { // https://bobbyhadz.com/blog/javascript-check-if-value-is-float
        console.log("id ei numero regexpin mukaan. Syötä int.");
        return false;
        // ongelmana muissa esim Number.isInteger in, että tunnistaa floatit ja numeroitakin sisältäviä.
        // sen lisäksi isSafeInt esim ei hyväksynyt suoraan stringiä ja parseInt hyväksyy kaikki, missä
        // on numeroita alussa. Tuskallista. Onko tähän parempaa tapaa? Pakko olla.
    }

    return true;
}


/**
  * Taso 3
  * Järjestää rastit taulukkoon aakkosjärjestykseen rastikoodin perustella siten, että
  * numeroilla alkavat rastit ovat kirjaimilla alkavien jälkeen.
  * Esim. jos rastikoodit ovat 9B, 8A, 99, foobar, niin oikea järjestys olisi foobar, 8A, 99, 9B
  * isoilla ja pienillä kirjaimilla ei ole järjestämisessä merkitystä (case insensitive)
  * @param {Object} rastit - Objekti, jonka sisältämistä rastiobjekteista muodostetaan järjestetty taulukko
  * @return {Array} palauttaa järjestetyn taulukon, joka sisältää kaikki rastiobjektit. Rastiobjektit ovat muotoa:
   {
   "id": rastit-objektissa käytetty kunkin rastiobjektin avain
   "koodi": rastikoodi merkkijonona
   "lat:: latitude liukulukuna
   "lon": longitude liukulukuna
   }
  */
function jarjestaRastit(rastit) {
    //console.log("jarjestaRastit", rastit);

    // lähetetään ensin rastit arrayn muodostukseen
    let kootutRastit = rastitToArray(rastit);

    // sitten sortataan tehty array.
    // En keksi muuta kuin tarkistaa eka merkki kirjain/numero
    // ja sortata ne erikseen ja sitten vasta yhdistää.
    // lienee parempiakin keinoja?
    let sortedKootutRastit = ekaMerkkiRastiKoodi(kootutRastit);
    //console.log("onko sorted rastit?", sortedKootutRastit);

    return sortedKootutRastit; // tässä pitää palauttaa järjestetty taulukko rasteista
}

/**
 * Kerää rastit objektin objektit listaan objekteiksi [{id, koodi, lat, lon}]
 * ja palauttaa listan.
 * @param {Object} rastit - Objekti, jonka sisältämistä rastiobjekteista muodostetaan taulukko
 * @return {Array} palauttaa luodun rastit taulukon.
 */
function rastitToArray(rastitObj) {
    let rastitArray = []; // palautettava array

    for (let avainId in rastitObj) { //käydään läpi kaikki objektit
        //console.log("wat?", avainId, rastitObj[avainId].koodi); // testitulostus löytyykö kaikki
        let rastiTesti = { // tehdään objekti objektista listaan lisättäväksi.
            id: avainId,
            koodi: rastitObj[avainId].koodi,
            lat: rastitObj[avainId].lat,
            lon: rastitObj[avainId].lon
        };
        rastitArray.push(rastiTesti); // pusketaan objekti palautettavaan listaan
    }
    
    //console.log(rastitArray); //testiprintti

    return rastitArray;
}

/**
 * jakaa rastit listan rastit kirjaimella alkaviin ja numeroilla alkaviin
 * sorttaa molemmat listat ja yhdistää numerolistan kirjainlistan perään.
 * @param {Array} rastitArray - rastit objektista tehty lista rasteja
 * @return {Array} listan sortattu kopio.
 */
function ekaMerkkiRastiKoodi(rastitArray) {

    // copypastesin vahingossa viime suorituskertani tarkistettavaa koodia, enkä omaani.
    // Saman tyylinen tapa meillä oli molemmilla, mutta ahdistaa kopioida toisen koodia,
    // joten korjasin mokani nyt kuitenkin.
    
    let kaikkiRastit = rastitArray;
    let kirjRastit = [];
    let numRastit = [];
    
    for (let i = 0; i < kaikkiRastit.length; i++) {
        // aika ovela https://stackoverflow.com/a/32567789
        if (kaikkiRastit[i].koodi[0].toLocaleLowerCase() !== kaikkiRastit[i].koodi[0].toLocaleUpperCase()) {
            kirjRastit.push(kaikkiRastit[i]);
        }
        else {
            numRastit.push(kaikkiRastit[i]);
        }
    }

    kirjRastit.sort(sortRastitKoodi);
    numRastit.sort(sortRastitKoodi);
    let yhdistetty= kirjRastit.concat(numRastit);

    return yhdistetty;
}

/**
 *
 * järjestää rastit koodin mukaan järjestykseen
 * @param {String} a
 * @param {String} b
 * @return {int} 
   */
function sortRastitKoodi(a, b) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#sorting_array_of_objects
    let aa = a.koodi.trim().toLocaleUpperCase("fi"); // trimmataan päistä, isot kirjaimet ja vertaillaan vasta sitten.
    let bb = b.koodi.trim().toLocaleUpperCase("fi"); // ehkä siinä säästä jotain, jos sen tekee alussa kerran, eikä jokaisessa if?
    //console.log("testi trim locale", aa, bb);
    
    if (!aa) {
	// console.log("ei kelpaa?", a);
	return 1; // jos undefined, niin vikaksi?
    }

    if (aa.length === 0) {
	return 1; // jos pituus nolla, niin vikaksi?
    }

    if (aa < bb) {
	// console.log("a pienempi", a, b);
	return -1; // a jos pienempi, niin eteen
    }

    if (aa  > bb) {
	// console.log("b pienempi", a, b);
	return 1; // a jos suurempi, niin perälle.
    }

    return 0; // jos samat, niin samat.
}

/**
  * Taso 3
  * Lisää joukkueen data-rakenteeseen ja palauttaa muuttuneen datan
  * Joukkue lisätään vain jos kaikki seuraavat ehdot täyttyvät:
  *  - Toista samannimistä joukkuetta ei ole olemassa. Nimien vertailussa
  *    ei huomioida isoja ja pieniä kirjaimia tai nimen alussa ja lopussa välilyöntejä etc. (whitespace). Nimien vertailu on siis caseinsensitive.
  *    "  Foo " on siis sama kuin "foo"
  *    Joukkueen nimi ei voi olla pelkkää whitespacea.
  *  - Leimaustapoja on annettava vähintään yksi kappale. Leimaustapojen
  *     on löydyttävä data.leimaustavat-taulukosta
  *  - Jäseniä on annettava vähintään kaksi kappaletta.
  *  - Saman joukkueen jäsenillä ei saa olla kahta samaa nimeä (caseinsensitive)
  *  - Sarjan id, jota vastaava sarja on löydyttävä data.sarjat-objektin sarjoista
  *
  *  Uusi joukkue tallennetaan data.joukkueet-taulukkoon. Joukkueen on oltava seuraavaa muotoa:
  *  {
  *     "id": {Number}, // jokaisella joukkueella oleva uniikki kokonaislukutunniste
  *     "nimi": {String}, // Joukkueen uniikki nimi
  *     "jasenet": {Array}, // taulukko joukkueen jäsenien nimistä
  *     "leimaustapa": {Array}, // taulukko joukkueen leimaustapojen indekseistä (kts. data.leimaustavat)
  *     "rastileimaukset": {Array}, // taulukko joukkueen rastileimauksista. Oletuksena tyhjä eli []
  *     "sarja": {Object}, // viite joukkueen sarjaan, joka löytyy data.sarjat-taulukosta
  *     "pisteet": {Number}, // joukkueen pistemäärä, oletuksena 0
  *     "matka": {Number}, // joukkueen kulkema matka, oletuksena 0
  *     "aika": {String}, // joukkueen käyttämä aika "h:min:s", oletuksena "00:00:00"
  *  }
  * @param {Object} Objekti, jonka joukkueet-taulukkoon joukkue lisätään
  * @param {String} nimi - Lisättävän joukkueen nimi
  * @param {Array} leimaustavat - Taulukko leimaustavoista
  * @param {String} sarja - Joukkueen sarjan id-tunniste
  * @param {Array} jasenet - joukkueen jäsenet
  * @return {Object} palauttaa aluperäisen datan ?? palauttaa uuden datan eikös?
  */
function lisaaJoukkue(data, nimi, leimaustavat, sarja, jasenet) {
    // console.log("lisaaJoukkue", data.joukkueet);

    // ensin id, eli uusi numero vaan.
    let joukkueId = annaUusiId(data.joukkueet);
    // console.log("uusi joukkueId", joukkueId);

    // tarkistetaan ehdotetun nimen sopivuus
    if (!eiSamaaNimea(nimi, data)) {
        console.log("nimi ei kelpaa tai nimi löytyi jo tosiaan, palautetaan alkuperäinen data");
        return data;
    }

    // tarkistetaan leimaustavat
    if (!leimaustapaOk(leimaustavat, data.leimaustavat)) {
        console.log("Leimaustavoissa häikkää. Palautetaan alkuperäinen data");
        return data;
    }
    let leimaustapaIndeksit = keraaLeimaustapaIndexit(leimaustavat, data.leimaustavat);

    // tarkistetaan sarja/sarja id
    if (!sarjaIdOk(sarja, data.sarjat)) {
        console.log("sarja (id?) ei kelpaa, palautetaan alkuperäinen data.");
        return data;
    }

    // etsitään uudelle joukkueelle oikea sarjaviite
    let sarjaViite = etsiSarjaViite(sarja, keraaIdtSarjoista(data.sarjat));

    // tarkistetaan syötetyt jäsenet
    if (!jasenetOk(jasenet)) {
        console.log("jäsenissä jotain vikaa. Anna väh kaksi oikeaa nimeä jäseniksi, kiitos. Palautetaan alkuperäinen data.");
        return data;
    }

    // varmistetaan, ettei se vika tyhjä tule mukaan ja ettei ole tyhjiä merkkejä.
    let jasenetTrim = trimmaaJasenet(jasenet);

    // rakennetaan uusi joukkue
    let uusiJoukkue = rakennaJoukkue(joukkueId, nimi, jasenetTrim, leimaustapaIndeksit, sarjaViite, data.sarjat);

    data.joukkueet.push(uusiJoukkue);
    
    return data;
}

/**
 * kerää annetusta listasta joukkue-objekteja niiden nimet (.nimet)
 * Tämä olisi ollut kiva tehdä universaalina kerääjänä, mutta en osannut.
 * @param {Array} listaJoukkueista - lista objekteja (joukkueet)
 * @return {Array} keratytNimet - lista joukkueiden nimistä
 */
function keraaNimetJoukkueista(listaJoukkueista) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
    let keratytNimet = listaJoukkueista.map(({nimi})=> nimi);
    //console.log("toimiiko sarjakeräys?", keratytNimet);
    return keratytNimet;
}

/**
 * Tarkistetaan löytyykö ehdotettu joukkuenimi jo datasta.
 * tarkistaa täysin tyhjän varalta,
 * tekee shallow kopion, mistä pois tyhjät edestä ja perästä ja
 * tarkistaa onko nimi listassa (isoilla kirjaimilla).
 * @param {String} nimi - syötetty nimi.
 * @param {Object} data, objekti, missä kaikki data. 
 * @return {boolean} jos löytyy tai muuta vikka false, ei löydy niin true.
 */
function eiSamaaNimea(nimi, data) {
    // tarkistetaan onko nimi vain white spacea.
    // https://stackoverflow.com/a/57564994
    if (!nimi.trim()) {
        console.log("tyhjä joukkuenimi? ", nimi);
        return false;
    }

    // trimmataan tyhjät pois
    let nimiTrim = nimi.trim();

    let joukkueNimet = keraaNimetJoukkueista(data.joukkueet);
    
    // tarkistetaan, jos sama nimi löytyy jo listasta ja palautetaan false, jos löytyy.
    for (let joukkueNimi of joukkueNimet) {
	if (joukkueNimi.toLocaleUpperCase("fi") === nimiTrim.toLocaleUpperCase("fi")) {
            console.log("joukkuenimi löytyy jo. Yritä toista.");
	    return false;
	}
    }
    console.log("joukkue nimeä ei vielä löytynyt, kelpaa");
    return true;
}

/**
 * Tarkistaa, onko leimaustapa ok
 * @param {Array} leimaustavat - ehdotettu leimaustapa
 * @param {Array} listaLeimaustavoista - lista hyväksytyistä leimaustavoista
 * @return {Boolean} true, jos ok. False, jos ei ok tyhjä.
 */
function leimaustapaOk(leimaustavat, listaLeimaustavoista) {
    // https://www.freecodecamp.org/news/check-if-javascript-array-is-empty-or-not-with-length/
    if (!leimaustavat.length) {
        console.log("ei yhtään syötettyä leimaustapaa, ei kelpaa");
        return false;
    }

    for (let leimaus of leimaustavat) {
        if (!leimaus.trim()) {
	    console.log("leimaustapa tyhjä, ei kelpaa");
	    return false;
        }
        if (!listaLeimaustavoista.includes(leimaus)) {
            console.log("annettu leimaustapa ei löydy hyväksyttyjen listalta, ei kelpaa");
            return false;
        }
    }
    console.log("leimaustavat löytyy hyväksyttyjen listalta, kelpaa.");
    return true;
}

/**
 * kerää listaan syötettyjen leimaustapojen ideksit työstettäväksi lisaaJoukkueessaa
 * @param {Array} leimaustavat - lista syötetyistä leimaustavoista
 * @param {Array} dataLeimaustavat - lista datan leimaustavoista
 * @return {Array} leimausIndexit - lista syötettyjen leimaustapojen indekseistä
 **/
function keraaLeimaustapaIndexit(leimaustavat, dataLeimaustavat) {
    let leimausIndexit = [];
    for (let leimaustapa of leimaustavat) {
        let indexi = etsiLeimaustapaIndex(leimaustapa, dataLeimaustavat);
        if (indexi != -1) { 
            leimausIndexit.push(indexi);
        }
    }
    return leimausIndexit;
}

/**
 * etsii listasta syötetyn leimauksen indeksin ja palauttaa tämän indeksin.
 * @param {String} leimaus - syötetty leimaustapa
 * @param {Array} leimaustavat - lista leimaustavoista
 * @return {Number} loydettyIndex - palauttaa indexin.
 */
function etsiLeimaustapaIndex(leimaus, leimaustavat) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex
    const haeIndeksi = (element) => element == leimaus;
    let loydettyViite = leimaustavat.findIndex(haeIndeksi);
    // console.log("löytyikö oikea leimausviite?", loydettyViite, leimaus, leimaustavat);
    return loydettyViite;
    
}

/**
 * Tarkistaa, onko sarjaId ok
 * @param {Array} id - syötetyt sarjaIdt
 * @param {Array} listaSarjoista - lista sarja objekteja
 * @return {Boolean} true, jos ok. False, jos ei ok tyhjä.
 */
function sarjaIdOk(id, sarjat) {
    //console.log("sarjaId ehdotettu? lienee stringi? ", id);
    let sarjaIdt = keraaIdtSarjoista(sarjat);

    if (!sarjaIdt.includes(parseInt(id))) {
        console.log("ehdotettu sarja id ei löydy hyväksyttyjen listalta, ei kelpaa.", id);
        return false;
    }
    console.log("ehdotettu sarja id löytyy hyväksyttyjen listalta, kelpaa.", id);
    return true;
}

/**
 * kerää annetusta listasta sarjat-objekteja sarjojen idt.
 * Tämä olisi ollut kiva tehdä universaalina kerääjänä, mutta en osannut.
 * @param {Array} listaSarjoista - lista objekteja (sarjat)
 * @return {Array} keratytIdt - lista sarjojen idtä (int?)
 */
function keraaIdtSarjoista(sarjat) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map
    let keratytIdt = sarjat.map(({id})=> id);
    // console.log("toimiiko sarjaid keräys?", keratytIdt);
    return keratytIdt;
}

/**
 * etsii data.sarjat listasta syötetyn sarja idn ja palauttaa tämän indeksin.
 * @param {String} sarja - syötetty sarjaid jostain syystä string
 * @param {Array} sarjaIdt - lista hyväksytyistä sarjaIdstä.
 * @return {String} loydettyViite - palauttaa viitteen suoraan.
 */
function etsiSarjaViite(sarja, sarjaIdt) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex
    const haeIndeksi = (element) => element == parseInt(sarja);
    let loydettyViite = sarjaIdt.findIndex(haeIndeksi);
    // console.log("löytyikö oikea viite?", loydettyViite);
    return loydettyViite;
    
}

/**
 * Tarkistaa, kelpaako syötetyt jäsenet (lista) ja palauttaa true/false
 * @param {Array} jasenet - syötetyt jäsenet
 * @return {Boolean} true, jos ok. False, jos ei ok.
 */
function jasenetOk(jasenet) {
    // parturoidaan tyhjät merkit alusta ja lopusta ja kokonaan tyhjät syötteet pois.
    // tehdään uusi lista hyväksyttävistä jäsenistä.
    let jasenetTrimUpper = trimmaaUpperJasenet(jasenet);


    // onko listan pituus oikea, eli onko hyväksyttyjä nimiä vähintään kaksi.
    if (jasenetTrimUpper.length < 2) {
        console.log("hyväksyttäviä jäsennimiä alle kaksi, ei kelpaa", jasenetTrimUpper);
        return false;
    }

    //tarkistetaan onko samoja nimiä. Tehdään set listasta ja verrataan kokoja. Jos eroja, ei kelpaa.
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set#remove_duplicate_elements_from_the_array
    let jasenetTrimUpperSet = new Set(jasenetTrimUpper);

    if (jasenetTrimUpper.length != jasenetTrimUpperSet.size) {
        console.log("samoja jäsennimiä, ei kelpaa.", jasenetTrimUpper, jasenetTrimUpperSet);
        return false;
    }
    
    console.log("jäseniä oikea määrä ja oikeanlaiset. Kelpaa.");
    return true;
}

/**
 * Trimmaa annetusta listasta stringit, lisää vain ei-tyhjät stringit uuteen listaan,
 * ja palauttaa uuden listan vertailtavaksi (tolocaleuppercase).
 * @param {Array} jasenlista - syötetyt, trimmattavat jäsenet
 * @return {Array} jasenetTrim - trimmatut, hyväksytyt jäsenet.
 */
function trimmaaUpperJasenet(jasenlista) {
    let jasenetTrimUpper = [];

    for (let jasen of jasenlista) {
        let jasenTrim = jasen.trim();
        if (jasenTrim.length > 0) {
            jasenetTrimUpper.push(jasenTrim.toLocaleUpperCase("fi"));
        }
    }
    // console.log("hyväksyttäviä jäseniä löytyi: ", jasenetTrimUpper);
    return jasenetTrimUpper;
}

/**
 * Trimmaa annetusta listasta stringit, lisää vain ei-tyhjät stringit uuteen listaan,
 * ja palauttaa uuden listan käytettäväksi joukkueen luomisessa.
 * @param {Array} jasenlista - syötetyt, trimmattavat jäsenet
 * @return {Array} jasenetTrim - trimmatut, hyväksytyt jäsenet.
 */
function trimmaaJasenet(jasenlista) {
    let jasenetTrim = [];

    for (let jasen of jasenlista) {
        let jasenTrim = jasen.trim();
        if (jasenTrim.length > 0) {
            jasenetTrim.push(jasenTrim);
        }
    }
    return jasenetTrim;
}

/**
 * Rakentaa uuden ja palauttaa uuden joukkue-objektin lisättäväksi dataan seuraavien ohjeiden mukaisesti:
 *
 *  Uusi joukkue tallennetaan data.joukkueet-taulukkoon. Joukkueen on oltava seuraavaa muotoa:
 *  {
 *     "id": {Number}, // jokaisella joukkueella oleva uniikki kokonaislukutunniste
 *     "nimi": {String}, // Joukkueen uniikki nimi
 *     "jasenet": {Array}, // taulukko joukkueen jäsenien nimistä
 *     "leimaustapa": {Array}, // taulukko joukkueen leimaustapojen indekseistä (kts. data.leimaustavat)
 *     "rastileimaukset": {Array}, // taulukko joukkueen rastileimauksista. Oletuksena tyhjä eli []
 *     "sarja": {Object}, // viite joukkueen sarjaan, joka löytyy data.sarjat-taulukosta
 *     "pisteet": {Number}, // joukkueen pistemäärä, oletuksena 0
 *     "matka": {Number}, // joukkueen kulkema matka, oletuksena 0
 *     "aika": {String}, // joukkueen käyttämä aika "h:min:s", oletuksena "00:00:00"
 *  }
 * @param {Number} uusiId, jonka joukkueet-taulukkoon joukkue lisätään
 * @param {String} uusiNimi- Lisättävän joukkueen nimi
 * @param {Array} uusiJasenet joukkueen jäsenet
 * @param {Array} uusiLeimaustavat - Taulukko leimaustavoista
 * @param {String} uusiSarjaViite - viite, indeksinumero data.sarjat objektista
 * @param {Array} dataSarjat - lista sarja objekteja
 * @return {Object} uusiJoukkue - palauttaa joukkueobjektin
 */
function rakennaJoukkue(uusId, uusiNimi, uusiJasenet, uusiLeimaustavat, uusiSarjaViite, dataSarjat) {
   let uusiJoukkue = {
        "id": uusId,
        "nimi": uusiNimi,
        "jasenet": uusiJasenet,
        "leimaustapa": uusiLeimaustavat,
        "rastileimaukset": [],
        "sarja": dataSarjat[uusiSarjaViite],
        "pisteet": 0,
        "matka": 0,
        "aika": "00:00:00"
   };
    // console.log("luotiin uusi joukkue: ", uusiJoukkue);
    return uusiJoukkue;
}

/**
  * Taso 3
  * Laskee joukkueen käyttämän ajan. Tulos tallennetaan joukkue.aika-ominaisuuteen.
  * Käytä merkkijonoa, jossa aika on muodossa "hh:mm:ss". Esim. "07:30:35"
  * Jos aikaa ei kyetä laskemaan, funktio palauttaa tyhjän merkkijonon ""
  * Aika lasketaan viimeisestä (ajan mukaan) LAHTO-rastilla tehdystä leimauksesta alkaen aina
  * ensimmäiseen (ajan mukaan) MAALI-rastilla tehtyyn leimaukseen asti. Leimauksia jotka tehdään
  * ennen viimeistä lähtöleimausta tai ensimmäisen maalileimauksen jälkeen ei huomioida.
  * @param {Object} joukkue
  * @return {Object} joukkue
  */
function laskeAika(joukkue) {

    //voidaanko aika laskea?
    if (!joukkueenRastitOk(joukkue, joukkue.rastileimaukset)) {
        return "";
    }

    // kerätään lähdöt ja maalit
    let lahdotMaalit = keraaLahdotMaalit(joukkue);
    //console.log(lahdotMaalit);

    // sortataan mahdolliset extra lahdot ja maalit varmuuden vuoksi?
    lahdotMaalit = sortLahdotMaalit(lahdotMaalit);
    //console.log(lahdotMaalit);

    // sitten yritetään laskea maalin ja lahdon erotus ms..
    let aika = laskeLahtoMaaliErotus(lahdotMaalit);
    //console.log(joukkue.nimi, "aika: ", aika);
    //console.log(joukkue.nimi, "aika date?: ", new Date(aika));
    // sitten lasketaan ms takaisin HH:mm:ss muotoon
    aika = msToHhMmSs(aika);
    //console.log(joukkue.nimi, "aika oikeassa muodossa?: ", aika);

    joukkue.aika = aika;
    //console.log(joukkue.nimi, "uusi aika on: ", joukkue.aika);

    return joukkue;
}

/**
 * testaa onko rastileimaukset ok. JoukkueRastiLeimaukset ei saa olla tyhjä,
 * eikä rastileimaukset.rasti väärin/olematon/undefined.
 * Palauttaa true/false
 * @param {Object} joukkue - joukkue objekti. Vähän varmuuden vuoksi. printtailua varten.
 * @param {Array} joukkueRastileimaukset - itse rastileimaukset lista
 * @return {Boolean}
 */
function joukkueenRastitOk(joukkue, joukkueRastileimaukset) {
    // jostain syystä, jos rastileimaus array on tyhjä tulee erroria undefined..
    // sama jos rastileimaus.rasti on tyhjä vaiko rsti? turhauttavaa.
    // kokeillaan raakata ne pois?
    if (joukkueRastileimaukset.length === 0) {
        //console.log("joukkueen ", joukkue.nimi, " rastileimaukset tyhjä: ", joukkueRastileimaukset);
        return false;
    }
        for (let x of joukkueRastileimaukset) {
            if (x.rasti == undefined) {
                //console.log("joukkueen ", joukkue.nimi, " rastileimaukset.rasti jotain vikaa: ", joukkueRastileimaukset);
                return false;
        }
    }

    return true;
}

/**
 * Muodostaa lahdotMaalit objektin palautettavaksi.
 * Objektiin kerätään joukkueen rastileimauksista lähdöt ja maalit ja ajat.
 * @param {Array} joukkue - lista joukkue objekteja
 * @return {Object} lahdotMaalit - nimi, lahdot, maalit
 */
function keraaLahdotMaalit(joukkue) {
    let nimi = joukkue.nimi;
    let lahdot = [];
    let maalit = [];

    for (let leimaus of joukkue.rastileimaukset) {
        if (leimaus.rasti.koodi === "LAHTO") {
            lahdot.push({"aika": leimaus.aika, "koodi": leimaus.rasti.koodi});
        }
        if (leimaus.rasti.koodi === "MAALI") {
            maalit.push({"aika": leimaus.aika, "koodi": leimaus.rasti.koodi});
        }
    }

    let lahdotMaalit = {"nimi": nimi,
                        "lahdot": lahdot,
                        "maalit": maalit
                       };
    
    return lahdotMaalit;
}

/**
 * järjestää toivott lähdöt ja maalit aika järjestykseen?
 * @param {Object} lahdotMaalit - Objekti joukkueiden lahdoista ja maaleista
 * @return {Object} lahdotMaalit - lopulta sortattu objekti maalit ja lahdot
   */
function sortLahdotMaalit(lahdotMaalit) {

    //console.log("sortLahdotMaalit lahdot?", lahdotMaalit.nimi, lahdotMaalit.lahdot.length);
    //console.log("sortLahdotMaalit maalit?", lahdotMaalit.nimi, lahdotMaalit.maalit.length);

    if (lahdotMaalit.lahdot.length > 1) {
        //console.log("sortataan ", lahdotMaalit.nimi, "lahdot", lahdotMaalit.lahdot);
        lahdotMaalit.lahdot.sort(localeCompareLahdotMaalit);
        //console.log(lahdotMaalit.nimi, "lahdot sortattu", lahdotMaalit.lahdot);
    }

    if (lahdotMaalit.maalit.length > 1) {
        //console.log("sortataan ", lahdotMaalit.nimi, "maalit", lahdotMaalit.maalit);
        lahdotMaalit.maalit.sort(localeCompareLahdotMaalit);
        //console.log(lahdotMaalit.nimi, "maalit sortattu", lahdotMaalit.maalit);
    }

    return lahdotMaalit;
}

/**
 *
 * järjestää toivott lähdöt ja maalit aika järjestykseen?
 * @param {String} a
 * @param {String} b
 * @return {int} 
   */
function localeCompareLahdotMaalit(a, b) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/localeCompare#numeric_sorting
    //console.log("sorttaako lahdot maalit? ", a, b);
    if (a.aika.localeCompare(b.aika, undefined, {numeric: true}) === -1) {
	//console.log("a pienempi?", a, b);
	return -1; // a jos pienempi, niin eteen
    }

    if (a.aika.localeCompare(b.aika, undefined, {numeric: true}) === 1) {
	//console.log("b pienempi", a, b);
	return 1; // a jos suurempi, niin perälle.
    }

    return 0; // jos samat, niin samat.
}
/**
  * Aika lasketaan viimeisestä (ajan mukaan) LAHTO-rastilla tehdystä leimauksesta alkaen aina
  * ensimmäiseen (ajan mukaan) MAALI-rastilla tehtyyn leimaukseen asti. Leimauksia jotka tehdään
  * ennen viimeistä lähtöleimausta tai ensimmäisen maalileimauksen jälkeen ei huomioida.
  * riittääkö, jos laskee siis vain maalin ja lahdon erotuksen vai joutuuko oikeasti keräämään
  * kaikki ja laskemaan?
  * @param {Object} lahdotMaalit
  * @returns {Date} maalin ja lahdon erotus.
  */
function laskeLahtoMaaliErotus(lahdotMaalit) {
    let dateLahto = dateLahtoMaali(lahdotMaalit.lahdot[lahdotMaalit.lahdot.length -1].aika);
    //console.log("LAHTO", dateLahto);
    let dateMaali = dateLahtoMaali(lahdotMaalit.maalit[0].aika);
    //console.log("MAALI", dateMaali);
    return dateMaali - dateLahto;
}

/**
 * ottaa stringin mallia "YY-MM-DD HH:mm:ss" ja palauttaa siitä Daten laskettavaksi
 * @param {String} aikaString - lahto tai maali aika stringi
 * @returns {Date} uusiDatePalautt - Date muotoon väännetty aikaString
   */
function dateLahtoMaali(aikaString) {
   // console.log("dateLahtoMaali testailua1 alku: ", aikaString);
    let uusiDate = aikaString.split(" ");
   // console.log("dateLahtoMaali testailua2 ' ' : ", uusiDate);
   // console.log(uusiDate);

    //https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/Strings#concatenating_strings
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/Date
    return new Date(`${uusiDate[0]}T${uusiDate[1]}`);
}

/**
 * muuntaa annetun numeron (ms) ja palauttaa ajan stringin muodossa
 * "HH:mm:ss". 
 * @param {Number} msInt - ms joukkueen maali-lahto ajasta.
 * @returns {String} HH:mm:ss 
 */
function msToHhMmSs(msInt) {
    // https://stackoverflow.com/a/69590637
    
    // ms sekunneiksi
    let ss = msInt / 1000;
    // sek tunneiksi
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/trunc
    let hh = Math.trunc(ss / 3600);
    // muutetaan sekunnit, mitä jää tunneista jäljelle.
    ss = ss % 3600;
    // minuutit jäljelle jääneistä sekunneista
    let mm = Math.trunc(ss / 60);
    // lopulliset jäljelle jääneet sekunnit:
    ss = ss % 60;

    // muokataan yksiköt, niin että on tarpeelliset nollatkin edessä (ja ovat taas stringejä).
    // javascript on outo.
    hh = ("00" + hh).slice(-2); // slice jättää vain perimmäiset kaksi.
    mm = ("00" + mm).slice(-2);
    ss = ("00" + ss).slice(-2);

    
    // sitten lähetetään tarvittavat stringi takaisin:
    return hh + ":" + mm + ":" + ss;
}

/**
  * Taso 3 ja Taso 5
  *  Järjestää joukkueet järjestykseen haluttujen tietojen perusteella
  *  järjestetään ensisijaisesti kasvavaan aakkosjärjestykseen
  *  mainsort-parametrin mukaisen tiedon perusteella. mainsort voi olla joukkueen nimi, sarjan nimi, matka, aika tai pisteet
  *  Järjestäminen on tehtävä alkuperäisen taulukon kopiolle. Alkuperäistä ei saa muuttaa tai korvata.
  *  Joukkueen jäsenet järjestetään aina aakkosjärjestykseen. Alkuperäisen joukkueobjektin jäsenten järjestys ei saa muuttua.
  *  Joukkueen leimaustavat järjestetään myös aina aakkosjärjestykseen leimaustapojen nimien mukaan
  *  Isoilla ja pienillä kirjaimilla ei ole missään järjestämisissä merkitystä (case insensitive) eikä myöskään alussa tai lopussa olevalla whitespacella. Vertailu on siis caseinsensitive.
  *  sortorder-parametrin käsittely vain tasolla 5
  *  jos sortorder-parametrina on muuta kuin tyhjä taulukko, käytetään
  *  sortorderin ilmoittamaa järjestystä eikä huomioida mainsort-parametria:
  *  ensisijaisesti järjestetään taulukon ensimmäisen alkion tietojen perusteella,
  *  toissijaisesti toisen jne.
  *  sortorder-taulukko sisältää objekteja, joissa kerrotaan järjestysehdon nimi (key),
  *  järjestyssuunta (1 = nouseva, -1 = laskeva) ja järjestetäänkö numeerisesti (true)
  *  vai aakkosjärjestykseen (false)
  *  Toteuta sortorder-taulukon käsittely siten, että taulukossa voi olla vaihteleva määrä rivejä
  *  Sarja täytyy huomioida erikoistapauksena
  *	 sortorder = [
  *	 {"key": "sarja", "order": 1, "numeric": false},
  *	 {"key": "nimi", "order": 1, "numeric": false},
  *	 {"key": "matka", "order": -1, "numeric": true},
  *	 {"key": "aika", "order": 1, "numeric": false},
  *	 {"key": "pisteet", "order": -1, "numeric": true}
  *	]
  * @param {Object} data - tietorakenne, jonka data.joukkueet-taulukko järjestetään
  * @param {String} mainsort - ensimmäinen (ainoa) järjestysehto, joka voi olla nimi, sarja, matka, aika tai pisteet  TASO 3
  * @param {Array} sortorder - mahdollinen useampi järjestysehto TASO 5
  * @return {Array} palauttaa järjestetyn ja täydennetyn _kopion_ data.joukkueet-taulukosta
  *
  */
function jarjestaJoukkueet(data, mainsort="nimi", sortorder=[]) {

    
    let dataJoukkueetCopy = Array.from(data.joukkueet);
    //console.log("ISOD eli dataCopy: ", dataJoukkueetCopy);
    //console.log("mainsort on nyt? ", mainsort); // miksi se on aina sarja? kun pitäisi olla nimi
    // console.log("sortorder length ", sortorder.length, "näyttänee 5, mikä on 'väärin', koska se on []");

    //TESTATAAS ETTÄ MIKÄ TÄSSÄ NYT ON MAINSORTISSA JA SORTORDERISSA VIKANA
    //mainsort = "matka";
    //sortorder = [];
    //console.log("mainsort on nyt manuaalisen vaihdon jälkeen? ", mainsort); // prkl
    //console.log("sortorder length ", sortorder.length, "näyttänee nyt 0, koska manuaalisesti korjattu");

    // console.log("sortattu nimien mukaan?", dataJoukkueetCopy.sort(sortJoukkueetNimien));
    // console.log("sortattu sarjan mukaan?", dataJoukkueetCopy.sort(sortJoukkueetSarjan));
    // console.log("sortattu matkan mukaan?", dataJoukkueetCopy.sort(sortJoukkueetMatkan));
    // console.log("sortattu ajan mukaan?", dataJoukkueetCopy.sort(sortJoukkueetAjan));
    // console.log("sortattu pisteiden mukaan?", dataJoukkueetCopy.sort(sortJoukkueetPisteiden));
    

    // joukkueen jäsenten aakkosjärjestys ihan vain ensimmäisestä nimestä.
    for (const joukkue of dataJoukkueetCopy) {
        //console.log("nyt sortataan joukkuetta ", joukkue);
        if (joukkue.jasenet.length > 1) {
            joukkue.jasenet.sort(sortJoukkueJasenet);
            //console.log("joukkue ", joukkue.nimi, "sortattu? ", joukkue.jasenet);
        }
            
    }

    // joukkueen leimaustapojen aakkostaminen. Nopea ja ruma.
    // ensin indeksit stringeiksi
    // sortataan stringit
    // muutetaan indekseiksi
    // korvataan joukkueen leimaustapalista tehdyllä indeksilistalla
    // melko kauhea.
    for (const joukkue of dataJoukkueetCopy) {
        if (joukkue.leimaustapa.length > 1) {
            // console.log(joukkue.leimaustapa);
            let joukkueLeimaustapaStringit = [];
            for (const i of joukkue.leimaustapa) {
                joukkueLeimaustapaStringit.push(data.leimaustavat[i]);
            }
            let leimausStringSorted = jarjestaLeimaustavat(joukkueLeimaustapaStringit);
            //console.log(leimausStringSorted);
            let leimausStringitIndekseiksi = keraaLeimaustapaIndexit(leimausStringSorted, data.leimaustavat);
            //console.log(leimausStringitIndekseiksi);
            joukkue.leimaustapa = leimausStringitIndekseiksi;
            // console.log(joukkue.leimaustapa);
        }
    }
    
    // TASO3 pitää tehdä ensin? ensin joukkueen nimen perusteella
    if (mainsort == "nimi") {
        dataJoukkueetCopy.sort(sortJoukkueetNimien);
        // näin voisi kääntää ehkä sitten sortorderissa?
        // dataJoukkueetCopy.sort((a,b) => {return (-1 * sortJoukkueetNimien(a,b))});
        // console.log("ISOD sortattu nimien mukaan? if laukaistu", dataJoukkueetCopy);
    }
    // sitten sarjan nimen perusteella
    if (mainsort === "sarja") {
        dataJoukkueetCopy.sort(sortJoukkueetSarjan);
        // console.log("ISOD sortattu sarjan mukaan? if laukaistu", dataJoukkueetCopy);
    }
    // sitten matkan pituuden perusteella
    if (mainsort === "matka") {
        dataJoukkueetCopy.sort(sortJoukkueetMatkan);
        // console.log("ISOD sortattu matkan mukaan? if laukaistu", dataJoukkueetCopy);
    }
    // ajan perusteella hh:mm:ss stringejä
    if (mainsort === "aika") {
        dataJoukkueetCopy.sort(sortJoukkueetAjan);
        // console.log("ISOD sortattu ajan mukaan? if laukaistu", dataJoukkueetCopy);
    }
    //pisteet suuruusjärjestykseen
    if (mainsort === "pisteet") {
        dataJoukkueetCopy.sort(sortJoukkueetPisteiden);
        // console.log("ISOD sortattu pisteiden mukaan? if laukaistu", dataJoukkueetCopy);
    }

    return dataJoukkueetCopy;
}

/**
 * sorttaa joukkueen jäsenlista aakkosjärjestykseen ensimmäisestä/ainoasta nimestä.
 * @param {String} a - verrattava nimi
 * @param {String} b - verrattava nimi
 * @returns {number} - -1, 0, 1
 */
function sortJoukkueJasenet(a, b) {
   // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#sorting_array_of_objects
    let aa = a.trim().toLocaleUpperCase("fi"); // trimmataan päistä, isot kirjaimet ja vertaillaan vasta sitten.
    let bb = b.trim().toLocaleUpperCase("fi"); // ehkä siinä säästä jotain, jos sen tekee alussa kerran, eikä jokaisessa if?
    //console.log("testi trim locale", aa, bb);

    if (!aa) {
	// console.log("ei kelpaa?", a);
	return 1; // jos undefined, niin vikaksi?
    }

    if (aa.length === 0) {
	return 1; // jos pituus nolla, niin vikaksi?
    }

    if (aa < bb) {
	// console.log("a pienempi", a, b);
	return -1; // a jos pienempi, niin eteen
    }

    if (aa  > bb) {
	// console.log("b pienempi", a, b);
	return 1; // a jos suurempi, niin perälle.
    }

    return 0; // jos samat, niin samat.
}


/**
 * ottaa listan joukkueita (objekteja) ja vääntää aakkosjärjestykseen
 * joukkue.nimi perusteella.
 * @param {String} a - verrattava joukkueen nimi
 * @param {String} b - verrattava joukkueen nimi
 * @returns {number} - -1, 0, 1
 */
function sortJoukkueetNimien(a, b) {
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#sorting_array_of_objects
    let aa = a.nimi.trim().toLocaleUpperCase("fi"); // trimmataan päistä, isot kirjaimet ja vertaillaan vasta sitten.
    let bb = b.nimi.trim().toLocaleUpperCase("fi"); // ehkä siinä säästä jotain, jos sen tekee alussa kerran, eikä jokaisessa if?
    //console.log("testi trim locale", aa, bb);
    
    if (!aa) {
	// console.log("ei kelpaa?", a);
	return 1; // jos undefined, niin vikaksi?
    }

    if (aa.length === 0) {
	return 1; // jos pituus nolla, niin vikaksi?
    }

    if (aa < bb) {
	// console.log("a pienempi", a, b);
	return -1; // a jos pienempi, niin eteen
    }

    if (aa  > bb) {
	// console.log("b pienempi", a, b);
	return 1; // a jos suurempi, niin perälle.
    }

    return 0; // jos samat, niin samat.
}

/**
 * ottaa listan joukkueita (objekteja) ja vääntää aakkosjärjestykseen
 * joukkue.sarja perusteella.
 * @param {String} a - verrattava sarja nimi
 * @param {String} b - verrattava sarja nimi
 * @returns {number} - -1, 0, 1
 */
function sortJoukkueetSarjan(a, b) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#sorting_array_of_objects
    let aa = a.sarja.nimi.trim().toLocaleUpperCase("fi"); // trimmataan päistä, isot kirjaimet ja vertaillaan vasta sitten.
    let bb = b.sarja.nimi.trim().toLocaleUpperCase("fi"); // ehkä siinä säästä jotain, jos sen tekee alussa kerran, eikä jokaisessa if?
    //console.log("testi trim locale", aa, bb);
    
    if (!aa) {
	// console.log("ei kelpaa?", a);
	return 1; // jos undefined, niin vikaksi?
    }

    if (aa.length === 0) {
	return 1; // jos pituus nolla, niin vikaksi?
    }

    if (aa < bb) {
	// console.log("a pienempi", a, b);
	return -1; // a jos pienempi, niin eteen
    }

    if (aa  > bb) {
	// console.log("b pienempi", a, b);
	return 1; // a jos suurempi, niin perälle.
    }

    return 0; // jos samat, niin samat.
}

/**
 * ottaa listan joukkueita (objekteja) ja vääntää pituusjärjestykseen
 * joukkue.matka perusteella.
 * @param {Number} a - verrattava matka numero
 * @param {Number} b - verrattava matka numero
 * @returns {Number} 0,1,-1 
 */
function sortJoukkueetMatkan(a, b) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#creating_displaying_and_sorting_an_array
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#sort_stability
    return a.matka - b.matka;
}

/**
 * ottaa listan joukkueita (objekteja) ja vääntää aakkosjärjestykseen
 * joukkue.aika perusteella.
 * @param {String} a - verrattava sarja aika
 * @param {String} b - verrattava sarja aika
 * @returns {number} - -1, 0, 1
 */
function sortJoukkueetAjan(a, b) {
    // eli ilmeisesti ehkä aika-stringiä voi verrata suoraan vaan? -->
    // https://stackoverflow.com/questions/6212305/how-can-i-compare-two-time-strings-in-the-format-hhmmss/6212411#6212411
    
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#sorting_array_of_objects
    let aa = a.aika.trim().toLocaleUpperCase("fi"); // trimmataan päistä, isot kirjaimet ja vertaillaan vasta sitten.
    let bb = b.aika.trim().toLocaleUpperCase("fi"); // ehkä siinä säästä jotain, jos sen tekee alussa kerran, eikä jokaisessa if?
    //console.log("testi trim locale", aa, bb);
    
    if (!aa) {
	// console.log("ei kelpaa?", a);
	return 1; // jos undefined, niin vikaksi?
    }

    if (aa.length === 0) {
	return 1; // jos pituus nolla, niin vikaksi?
    }

    if (aa < bb) {
	// console.log("a pienempi", a, b);
	return -1; // a jos pienempi, niin eteen
    }

    if (aa  > bb) {
	// console.log("b pienempi", a, b);
	return 1; // a jos suurempi, niin perälle.
    }

    return 0; // jos samat, niin samat.
}

/**
 * ottaa listan joukkueita (objekteja) ja vääntää numerojärjestykseen
 * joukkue.pisteet perusteella.
 * @param {Number} a - verrattava pisteet
 * @param {Number} b - verrattava pisteet
 * @returns {Number} 0,1,-1 
 */
function sortJoukkueetPisteiden(a, b) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#creating_displaying_and_sorting_an_array
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#sort_stability
    return a.pisteet - b.pisteet;
}

/**
  * Taso 5
  * Laskee joukkueen kulkeman matkan. Matka tallennetaan joukkue.matka-ominaisuuteen liukulukuna
  * Laske kuinka pitkän matkan kukin joukkue on kulkenut eli laske kunkin rastivälin
  * pituus ja laske yhteen kunkin joukkueen kulkemat rastivälit. Jos rastille ei löydy
  * sijaintitietoa (lat ja lon), niin kyseistä rastia ei lasketa matkaan mukaan. Matka
  * lasketaan viimeisestä LAHTO-rastilla tehdystä leimauksesta alkaen aina
  * ensimmäiseen MAALI-rastilla tehtyyn leimaukseen asti. Leimauksia jotka tehdään
  * ennen lähtöleimausta tai maalileimauksen jälkeen ei huomioida.
  * Käytä annettua apufunktiota getDistanceFromLatLonInKm
  * @param {Object} joukkue
  * @return {Object} joukkue
  */
function laskeMatka(joukkue) {
    return joukkue;
}

/**
  * Taso 5
  * Laskee joukkueen saamat pisteet. Pistemäärä tallennetaan joukkue.pisteet-ominaisuuteen
  * Joukkue saa kustakin rastista pisteitä rastin koodin ensimmäisen merkin
  * verran. Jos rastin koodi on 9A, niin joukkue saa yhdeksän (9) pistettä. Jos rastin
  * koodin ensimmäinen merkki ei ole kokonaisluku, niin kyseisestä rastista saa nolla
  * (0) pistettä. Esim. rasteista LÄHTÖ ja F saa 0 pistettä.
  * Samasta rastista voi sama joukkue saada pisteitä vain yhden kerran. Jos
  * joukkue on leimannut saman rastin useampaan kertaan lasketaan kyseinen rasti
  * mukaan pisteisiin vain yhden kerran.
  * Rastileimauksia, jotka tehdään ennen lähtöleimausta tai maalileimauksen jälkeen, ei
  * huomioida.
  * Maalileimausta ei huomioida kuin vasta lähtöleimauksen jälkeen.
  * Jos joukkueella on useampi lähtöleimaus, niin pisteet lasketaan vasta
  * viimeisen lähtöleimauksen jälkeisistä rastileimauksista.
  * Joukkue, jolla ei ole ollenkaan rastileimauksia, saa 0 pistettä
  * @param {Object} joukkue
  * @return {Object} joukkue
  */
function laskePisteet(joukkue) {
    return joukkue;
}



// apufunktioita tasolle 5
/**
  * Laskee kahden pisteen välisen etäisyyden
  */
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    let R = 6371; // Radius of the earth in km
    let dLat = deg2rad(lat2 - lat1);  // deg2rad below
    let dLon = deg2rad(lon2 - lon1);
    let a =
	Math.sin(dLat / 2) * Math.sin(dLat / 2) +
	Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c; // Distance in km
    return d;
}
/**
   Muuntaa asteet radiaaneiksi
  */
function deg2rad(deg) {
    return deg * (Math.PI / 180);
}
